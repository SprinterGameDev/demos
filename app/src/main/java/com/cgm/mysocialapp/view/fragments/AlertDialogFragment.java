package com.cgm.mysocialapp.view.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import com.cgm.mysocialapp.R;

/**
 * Created by admin on 20.10.15.
 */
public final class AlertDialogFragment extends DialogFragment {
    public static final String KEY_MESSAGE = "keyMessage";

    private String mMessage = "";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public final Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null)
            mMessage = bundle.getString(KEY_MESSAGE);

        AlertDialog.Builder dlgErrorBuilder = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.app_name)
                .setMessage(mMessage)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return dlgErrorBuilder.create();
    }
}
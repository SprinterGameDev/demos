package com.cgm.mysocialapp.view.fragments;

/**
 * Created by admin on 20.10.15.
 */
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cgm.mysocialapp.R;

public final class ProgressDialogFragment extends DialogFragment {
    public static final String KEY_CAPTION = "caption";
    private View mView;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String caption = getString(R.string.app_name);

        Bundle bundle = getArguments();
        if (bundle != null)
            caption = bundle.getString(KEY_CAPTION);

        getDialog().setTitle(caption);

        mView = inflater.inflate(R.layout.fragment_dialog_progress, container, false);

        return  mView;
    }
}
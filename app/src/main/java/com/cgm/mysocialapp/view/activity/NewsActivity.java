package com.cgm.mysocialapp.view.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.adapters.AdapterListNews;
import com.cgm.mysocialapp.collections.PostCollection;
import com.cgm.mysocialapp.controllers.NewsActivityController;
import com.cgm.mysocialapp.models.PostItemModel;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by admin on 09.10.15.
 */
public final class NewsActivity extends AppCompatActivity {
    private static final int MAX_LINES_MESSAGE = 20;

    private NewsActivityController mController = null;
    private View mView = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        String modelId = getIntent().getStringExtra(AdapterListNews.NAME_EXTRA_PARSE_OBJECT_ID);

        mController = new NewsActivityController(this, modelId);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(R.layout.layout_item_news, null);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        mView.setVisibility(View.INVISIBLE);
        scrollView.addView(mView);

        if (modelId != null)
            fetchModel(modelId);
    }

    public final void setAvatar(Bitmap photo,ImageView view) {
        BitmapUtils.clearImageView(view);

        if (photo == null)
            return;

        view.setImageBitmap(
                BitmapUtils.getCroppedBitmap(photo)
        );
    }

    public final void setPhoto(ParseFile file, ImageView view) {
        BitmapUtils.clearImageView(view);

        if (file == null)
            return;

        try {
            byte[] bitmapData = file.getData();
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            view.setImageBitmap(bitmap);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private final void fetchModel(String id) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("CollectionPost");
        query.getInBackground(id, new GetCallback<ParseObject>() {
            public void done(ParseObject object, ParseException e) {
                if (e == null) {
                    updateView(new PostItemModel(object));
                } else {
                    Multitone.getInstance().getDlgManager().openCommonDialog(
                            getSupportFragmentManager(), e.getMessage());
                }
            }
        });
    }

    private final void updateView(PostItemModel model) {
        ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
        ImageView userPhoto = (ImageView) mView.findViewById(R.id.imgUserPhoto);
        ImageView imgPost = (ImageView) mView.findViewById(R.id.imgPost);
        TextView txtCreateAt = (TextView) mView.findViewById(R.id.txtCreateAt);
        TextView txtCaption = (TextView) mView.findViewById(R.id.label);
        TextView txtMessage = (TextView) mView.findViewById(R.id.txtMessage);
        Button btnComment = (Button) mView.findViewById(R.id.btnComment);
        Button btnLikes = (Button) mView.findViewById(R.id.btnLikes);
        Button btnShare = (Button) mView.findViewById(R.id.btnShare);

        setAvatar(model.getAvatar(), userPhoto);
        txtCaption.setText(model.getName());
        txtCreateAt.setText(model.getCreateAt());
        txtMessage.setMaxLines(MAX_LINES_MESSAGE);
        txtMessage.setText(model.getMessage());
        setPhoto(model.getPhoto(), imgPost);
        btnComment.setText(String.valueOf(model.getCommentCount()));
        btnLikes.setText(String.valueOf(model.getLikeCount()));
        btnShare.setText(String.valueOf(model.getShareCount()));

        progress.setVisibility(View.INVISIBLE);
        mView.setVisibility(View.VISIBLE);
    }
}

package com.cgm.mysocialapp.view.layouts;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.controllers.ProfileController;
import com.cgm.mysocialapp.models.UserModel;
import com.cgm.mysocialapp.utilits.BitmapUtils;

/**
 * Created by admin on 01.11.15.
 */
public final class ProfileLayout {
    private ProfileController mController = null;
    private View mView = null;
    private UserModel mUser;

    public ProfileLayout(AppCompatActivity activity, UserModel user) {
        mController = new ProfileController(activity, mUser);

        LayoutInflater mInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = (View) mInflater.inflate(R.layout.layout_profile, null, false);

        TextView txtName = (TextView) mView.findViewById(R.id.txtUserName);
        ImageView imagePhoto = (ImageView) mView.findViewById(R.id.imgPhoto);
        Button btnCreatePost = (Button) mView.findViewById(R.id.btnCreatePost);
        Button btnCreateImagePost = (Button) mView.findViewById(R.id.btnCreateImagePost);
        txtName.setText(user.getName());
        imagePhoto.setImageBitmap(
                BitmapUtils.getCroppedBitmap(user.getAvatar())
        );

        btnCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mController.openCreatePost();
            }
        });
    }

    public final View getView() {
        return mView;
    }

    public final void destroy() {
        mUser = null;
        mView = null;
        mController.destroy();
        mController = null;
    }
}

package com.cgm.mysocialapp.view.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.adapters.AdapterListNews;
import com.cgm.mysocialapp.collections.PostCollection;
import com.cgm.mysocialapp.utilits.callback.EventCallBack;

/**
 * Created by admin on 03.10.15.
 */
public final class NewsListFragment extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener {

    public static final String KEY_SHOW_INFO_USER = "showInfoUser";
    public static final String KEY_USER_ID = "userId";

    private SwipeRefreshLayout mSwipe = null;
    private View mView = null;
    private PostCollection mPostCollection = null;
    private AdapterListNews mAdapter = null;
    private String userId = null;

    public NewsListFragment() {
    }

    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        if (mView == null) {
            Bundle bundle = getArguments();
            if (bundle != null)
                userId = bundle.getString(KEY_USER_ID);

            mView = inflater.inflate(R.layout.fragment_list_news, container, false);
            ListView listView = (ListView) mView.findViewById(R.id.listNews);

            mSwipe = (SwipeRefreshLayout) mView.findViewById(R.id.swipeNews);
            mSwipe.setOnRefreshListener(this);
            mSwipe.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

            mPostCollection = new PostCollection();
            mAdapter = new AdapterListNews(userId != null, getActivity(), mPostCollection);
            mPostCollection.addEventListener(PostCollection.EVENT_SUCCESS_LOAD, onFetchCollection);

            listView.setAdapter(mAdapter);
            if (userId == null)
                mPostCollection.fetch();
            else
                mPostCollection.fetchByUser(userId);

        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        return  mView;
    }

    @Override
    public final void onRefresh() {
        if (userId == null)
            mPostCollection.fetch();
        else
            mPostCollection.fetchByUser(userId);
    }

    @Override
    public void onDestroy() {
        mPostCollection.removeEventListener(PostCollection.EVENT_SUCCESS_LOAD, onFetchCollection);
        mAdapter = null;
        super.onDestroy();
    }

    private EventCallBack onFetchCollection = new EventCallBack() {
        @Override
        public void onEvent(int code) {
            mAdapter.notifyDataSetChanged();
            mSwipe.setRefreshing(false);
        }
    };
}

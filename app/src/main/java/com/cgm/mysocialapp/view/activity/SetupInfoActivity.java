package com.cgm.mysocialapp.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.controllers.SetupInfoController;
import com.cgm.mysocialapp.models.UserModel;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.cgm.mysocialapp.utilits.callback.EventCallBack;

/**
 * Created by admin on 24.10.15.
 */
public final class SetupInfoActivity extends AppCompatActivity {
    private SetupInfoController mController = null;
    ImageView avatarView = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_setup_info);
        mController = new SetupInfoController(this);
        avatarView = (ImageView) this.findViewById(R.id.imgChoisePhoto);
        Button btnNext = (Button) this.findViewById(R.id.btnNext);
        final EditText txtName = (EditText) this.findViewById(R.id.txtName);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Multitone.getInstance().getAuth().getUser().setName(txtName.getText().toString());
                mController.applyInfo();
            }
        });

        avatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mController.pickupPhoto();
            }
        });

        Multitone.getInstance().getAuth().getUser().addEventListener(
                UserModel.EVENT_CHANGE_AVATAR, onChangePhoto);
    }

    // 2.0 and above
    @Override
    public final void onBackPressed() {
        moveTaskToBack(false);
    }

    // Before 2.0
    @Override
    public final boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SetupInfoController.PICK_PHOTO_FOR_AVATAR && resultCode == this.RESULT_OK) {
            if (data == null)
                return;

            mController.applyAvatarUser(data.getData());
        }
    }

    @Override
    protected final void onRestoreInstanceState(Bundle instance) {
        super.onRestoreInstanceState(instance);
        updateAvatar();
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        Multitone.getInstance().getAuth().getUser().removeEventListener(
                UserModel.EVENT_CHANGE_AVATAR, onChangePhoto);
    }

    private final EventCallBack onChangePhoto = new EventCallBack() {
        @Override
        public void onEvent(int result) {
            updateAvatar();
        }
    };

    private final void updateAvatar() {
        if (Multitone.getInstance().getAuth().getUser().getAvatar() == null)
            return;

        avatarView.setImageBitmap(
                BitmapUtils.getCroppedBitmap(
                        Multitone.getInstance().getAuth().getUser().getAvatar()
                )
        );
    }
}

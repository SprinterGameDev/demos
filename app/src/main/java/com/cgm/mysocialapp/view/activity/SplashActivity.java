package com.cgm.mysocialapp.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.controllers.SplashActivityController;

/**
 * Created by admin on 19.10.15.
 */
public final class SplashActivity extends AppCompatActivity {
    private SplashActivityController mController = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController = new SplashActivityController(this);

        setContentView(R.layout.activity_splash);
    }

    @Override
    protected final void onResume() {
        super.onResume();
        mController.init();
    }

    @Override
    protected final void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mController.saveInstance(outState);
    }

    @Override
    protected final void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mController.restoreInstance(savedInstanceState);
    }
}

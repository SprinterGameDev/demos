package com.cgm.mysocialapp.view.activity;

import android.app.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.controllers.AuthActivityController;


/**
 * Created by admin on 03.10.15.
 */
public final class AuthActivity extends AppCompatActivity {
    private AuthActivityController mController = null;
    private EditText mTxtLogin = null;
    private EditText mTxtPass = null;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mController = new AuthActivityController(this);
        this.findViewById(R.id.btnSignIn).setOnClickListener(btnOnClickSignIn);
        this.findViewById(R.id.btnSingUp).setOnClickListener(btnOnClickSignUp);
        mTxtLogin = (EditText) this.findViewById(R.id.txtLogin);
        mTxtPass = (EditText) this.findViewById(R.id.txtPassword);
    }

    // 2.0 and above
    @Override
    public final void onBackPressed() {
        moveTaskToBack(false);
    }

    // Before 2.0
    @Override
    public final boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected final void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mController.saveInstance(outState);
    }

    @Override
    protected final void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mController.restoreInstance(savedInstanceState);
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        mController.destroy();
    }

    private final View.OnClickListener btnOnClickSignIn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mController.signInUser(mTxtLogin.getText().toString(), mTxtPass.getText().toString());
        }
    };

    private final View.OnClickListener btnOnClickSignUp = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mController.signUpUser(mTxtLogin.getText().toString(), mTxtPass.getText().toString());
        }
    };
}

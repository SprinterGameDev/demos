package com.cgm.mysocialapp.view.activity;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;

import com.cgm.mysocialapp.controllers.MainActivityController;
import com.cgm.mysocialapp.view.fragments.NavigationDrawerFragment;
import com.cgm.mysocialapp.R;

public final class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = "MainActivity";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private MainActivityController mController = null;
    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private Boolean mRestored = true;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController = new MainActivityController(this);

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.mainLayout));
    }

    @Override
    public final void onNavigationDrawerItemSelected(int position) {
        if (mRestored == true) {
            mController.changeMenuNavigator(position);
            onSectionAttached(position);
        }
    }

    public final void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.menu_back_my_posts);
                break;
            case 1:
                mTitle = getString(R.string.menu_back_news);
                break;
            case 2:
                mTitle = getString(R.string.menu_back_settings);
                break;
        }
        restoreActionBar();
    }

    public final void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    protected final void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mController.saveInstance(outState);
    }

    @Override
    protected final void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mController.restoreInstance(savedInstanceState);
        mRestored = true;
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        mController.destroy();
        mRestored = false;
    }
}


package com.cgm.mysocialapp.view.fragments;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.controllers.CreatePostController;
import com.cgm.mysocialapp.controllers.SetupInfoController;
import com.shamanland.fonticon.FontIconView;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by admin on 26.10.15.
 */
public final class CreatePostDialogFragment extends DialogFragment {
    private final int RESULT_OK = -1;
    private View mView = null;
    private CreatePostController mController = null;
    private EditText mEditTextMessage = null;
    private FontIconView mPhotoView = null;

    public CreatePostDialogFragment() {
    }

    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle(R.string.create_post_caption);

        if (mView == null) {
            mController = new CreatePostController(this);
            mView = inflater.inflate(R.layout.fragment_create_post, container, false);
            mEditTextMessage = (EditText) mView.findViewById(R.id.txtPostMessage);
            FontIconView btnCreate = (FontIconView) mView.findViewById(R.id.btnCreatePost);
            mPhotoView = (FontIconView) mView.findViewById(R.id.imgAttachImage);
            mPhotoView.setOnClickListener(mClickAttachImage);
            btnCreate.setOnClickListener(mClickCreate);
        } else {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }

        return mView;
    }

    private final View.OnClickListener mClickCreate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mController.createPost(mEditTextMessage.getText().toString());
        }
    };

    private final View.OnClickListener mClickAttachImage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mController.pickupImage();
        }
    };

    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CreatePostController.PICK_PHOTO_ATTACH && resultCode == RESULT_OK) {
            if (data == null)
                return;
            try {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Drawable drawable = new BitmapDrawable(getResources(), BitmapFactory.decodeStream(inputStream));
                mPhotoView.setBackgroundDrawable(drawable);
                mPhotoView.setText("");
                mController.applyAttachImage(data.getData());
            } catch(FileNotFoundException fileNotFound) {
                Multitone.getInstance()
                        .getDlgManager()
                        .openCommonDialog(this.getFragmentManager(), fileNotFound.getMessage());
            }
        }
    }

    @Override
    public final void onDestroyView() {
        if (getDialog() != null && getRetainInstance())
            getDialog().setOnDismissListener(null);
        super.onDestroyView();
    }

    @Override
    public final void onDestroy() {
        mController.destroy();
        super.onDestroy();
    }
}

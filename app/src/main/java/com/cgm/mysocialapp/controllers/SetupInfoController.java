package com.cgm.mysocialapp.controllers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.cgm.mysocialapp.view.activity.MainActivity;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by admin on 24.10.15.
 */
public final class SetupInfoController extends AbstractController {
    public static final int PICK_PHOTO_FOR_AVATAR = 1;

    public SetupInfoController(AppCompatActivity activity) {
        super(activity);
    }

    public final void pickupPhoto() {
        Intent gallery = new Intent(Intent.ACTION_GET_CONTENT);
        gallery.setType("image/*");
        mActivity.startActivityForResult(gallery, PICK_PHOTO_FOR_AVATAR);
    }

    public final void applyAvatarUser(Uri uri) {
        try {
            InputStream inputStream = mActivity.getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            Bitmap bmp = BitmapUtils.getResizedBitmap(
                    BitmapFactory.decodeStream(inputStream, null, options), 100, 100);

            String bmpBase64 = BitmapUtils.encodeTobase64(bmp);
            if (Multitone.getInstance().getAuth().getUser() != null)
                Multitone.getInstance().getAuth().getUser().setAvatar(bmpBase64);
        } catch(FileNotFoundException fileNotFound) {
            Multitone.getInstance()
                    .getDlgManager()
                    .openCommonDialog(mActivity.getSupportFragmentManager(), fileNotFound.getMessage());
        }
    }

    public final void applyInfo() {
        if (Multitone.getInstance().getAuth().getUser().getName().length() > 0) {
            openMainActivity();
        } else {
            Multitone.getInstance()
                    .getDlgManager()
                    .openCommonDialog(
                            mActivity.getSupportFragmentManager(), mActivity.getString(R.string.setup_info_msg_name_empty)
                    );
        }
    }

    private final void openMainActivity() {
        Intent mainActivity = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(mainActivity);
        mActivity.overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
        mActivity.finish();
    }
}

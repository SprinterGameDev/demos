package com.cgm.mysocialapp.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.view.activity.AuthActivity;
import com.cgm.mysocialapp.view.activity.MainActivity;
import com.cgm.mysocialapp.view.activity.SetupInfoActivity;
import com.cgm.mysocialapp.view.activity.SplashActivity;
import com.parse.Parse;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by admin on 19.10.15.
 */
public final class SplashActivityController extends AbstractController {
    private static final int SPLASH_DISPLAY_LENGTH = 1000;

    public SplashActivityController(AppCompatActivity activity) {
        super(activity);
    }

    public final void init() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Multitone.getInstance().getAuth().isActive() == true) {
                    if (Multitone.getInstance().getAuth().getUser().getName().length() > 0) {
                        openMainActivity();
                    } else {
                        openSetupActivity();
                    }
                } else {
                    openAuthActivity();
                }
                mActivity.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    public final void saveInstance(Bundle outState) {
    }

    @Override
    public final void restoreInstance(Bundle savedInstance) {
    }

    private final void openAuthActivity() {
        Intent intent = new Intent(mActivity, AuthActivity.class);
        mActivity.startActivity(intent);
    }

    private final void openMainActivity() {
        Intent intent = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(intent);
    }

    private final void openSetupActivity() {
        Intent intent = new Intent(mActivity, SetupInfoActivity.class);
        mActivity.startActivity(intent);
    }
}

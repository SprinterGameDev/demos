package com.cgm.mysocialapp.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.auth.AuthAbstract;
import com.cgm.mysocialapp.utilits.callback.EventCallBack;
import com.cgm.mysocialapp.view.activity.MainActivity;
import com.cgm.mysocialapp.view.activity.SetupInfoActivity;
import com.cgm.mysocialapp.view.fragments.AlertDialogFragment;
import com.cgm.mysocialapp.view.fragments.ProgressDialogFragment;

/**
 * Created by admin on 03.10.15.
 */
public class AuthActivityController extends AbstractController {

    private static final String TAG = "AuthActivityController";

    public AuthActivityController(AppCompatActivity activity) {
        super(activity);

        Multitone.getInstance().getAuth().addEventListener(
                AuthAbstract.EVENT_AUTH_SUCCESS, onSuccessAuth);

        Multitone.getInstance().getAuth().addEventListener(
                AuthAbstract.EVENT_AUTH_ERROR, onErrorAuth);
    }

    public final void signInUser(String login, String pass) {
        openDlgProgress();
        Multitone.getInstance().getAuth().signIn(login, pass);
    }

    public final void signUpUser(String login, String pass) {
        openDlgProgress();
        Multitone.getInstance().getAuth().signUp(login, pass);
    }

    @Override
    public void saveInstance(Bundle outState) {
    }

    @Override
    public void restoreInstance(Bundle savedInstance) {
    }

    @Override
    public final void destroy() {
        mActivity = null;

        Multitone.getInstance().getAuth().removeEventListener(
                AuthAbstract.EVENT_AUTH_SUCCESS, onSuccessAuth);

        Multitone.getInstance().getAuth().removeEventListener(
                AuthAbstract.EVENT_AUTH_ERROR, onErrorAuth);
    }

    private final EventCallBack onErrorAuth = new EventCallBack() {
        @Override
        public void onEvent(int result) {
            Multitone.getInstance()
                    .getDlgManager().closeProgressDialog(mActivity.getSupportFragmentManager());

            Multitone.getInstance()
                    .getDlgManager()
                    .openCommonDialog(mActivity.getSupportFragmentManager(), mActivity.getString(R.string.auth_message_error));
        }
    };

    private final EventCallBack onSuccessAuth = new EventCallBack() {
        @Override
        public void onEvent(int result) {
            Multitone.getInstance()
                    .getDlgManager().closeProgressDialog(mActivity.getSupportFragmentManager());
            if (Multitone.getInstance().getAuth().getUser() != null &&
                    Multitone.getInstance().getAuth().getUser().getName().length() > 0) {
                openMainActivity();
            } else {
                openSetupActivity();
            }
            mActivity.finish();
        }
    };

    private final void openMainActivity() {
        Intent mainActivity = new Intent(mActivity, MainActivity.class);
        mActivity.startActivity(mainActivity);
        mActivity.overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
    }

    private final void openSetupActivity() {
        Intent mainActivity = new Intent(mActivity, SetupInfoActivity.class);
        mActivity.startActivity(mainActivity);
        mActivity.overridePendingTransition(R.anim.push_right_out, R.anim.push_right_in);
    }

    private final void openDlgProgress() {
        Multitone.getInstance()
                .getDlgManager()
                .openCommonProgressDialog(mActivity.getSupportFragmentManager(), mActivity.getString(R.string.auth_dialog_caption));
    }
}

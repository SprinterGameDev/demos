package com.cgm.mysocialapp.controllers;

import android.support.v7.app.AppCompatActivity;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.models.UserModel;

/**
 * Created by admin on 01.11.15.
 */
public final class ProfileController extends AbstractController {
    public ProfileController(AppCompatActivity activity, UserModel user){
        super(activity);
    }

    public final void openCreatePost() {
        Multitone.getInstance().getDlgManager().openCreatePost(mActivity.getSupportFragmentManager());
    }

    public final void openCreateImagePost() {

    }

    @Override
    public void destroy() {
        super.destroy();
    }
}

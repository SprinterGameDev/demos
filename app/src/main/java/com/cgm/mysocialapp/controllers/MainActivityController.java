package com.cgm.mysocialapp.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.view.activity.AuthActivity;
import com.cgm.mysocialapp.view.fragments.NavigationDrawerFragment;
import com.cgm.mysocialapp.view.fragments.NewsListFragment;

import java.util.List;

/**
 * Created by admin on 03.10.15.
 */
public final class MainActivityController extends AbstractController {
    public static final String TAG_FRAGMENT_MY_PAGE = "fragmentMyPosts";
    public static final String TAG_FRAGMENT_NEWS = "fragmentNews";
    public static final String TAG_FRAGMENT_SETTINGS = "fragmentSettings";

    private static final int PAGE_NAME_MY_PAGE = 0;
    private static final int PAGE_NAME_NEWS = 1;
    private static final int PAGE_NAME_USER = 2;

    private FragmentActivity mFragmentActivity = null;
    private Fragment mActiveFragment = null;

    private int mActivePage = -1;

    public MainActivityController(FragmentActivity activity) {
        super(null);
        mFragmentActivity = activity;
    }

    @Override
    public final void destroy() {
        mFragmentActivity = null;
        mActiveFragment = null;
    }

    @Override
    public final void saveInstance(Bundle outState) {
        outState.putInt("selectedMenu", mActivePage);
    }

    @Override
    public final void restoreInstance(Bundle savedInstance) {
        int selectedMenu = savedInstance.getInt("selectedMenu", 0);
        changeMenuNavigator(selectedMenu);
    }

    public final void changeMenuNavigator(int id) {
        String tag = "";
        Fragment result;

        if (id == mActivePage)
            return;

        switch (id) {
            case PAGE_NAME_MY_PAGE:
                tag = TAG_FRAGMENT_MY_PAGE;
               break;
            case PAGE_NAME_NEWS:
                tag = TAG_FRAGMENT_NEWS;
                break;
            case PAGE_NAME_USER:
                break;
            case NavigationDrawerFragment.INDEX_BTN_EXIT:
                logoutMe();
                return;
        }

        result = getFragment(tag);
        mActivePage = id;

        if (mActiveFragment != null)
            hideFragment(mActiveFragment);

        if (result == null)
            return;

        mActiveFragment = result;
        result.setRetainInstance(true);
        initFragment(mActiveFragment, tag);
    }

    private final Fragment getFragment(String tagName) {
        Fragment result = null;
        result = getByTagFragment(tagName);
        if (result != null)
            return  result;

        switch (tagName) {
            case TAG_FRAGMENT_MY_PAGE:
                NewsListFragment news = new NewsListFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean(NewsListFragment.KEY_SHOW_INFO_USER, true);
                bundle.putString(
                        NewsListFragment.KEY_USER_ID, Multitone.getInstance().getUser().getId());
                news.setArguments(bundle);
                return news;
            case TAG_FRAGMENT_NEWS:
                return new NewsListFragment();
        }

        return result;
    }

    private final void initFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager;
        fragmentManager = mFragmentActivity.getSupportFragmentManager();

        List<Fragment> list = fragmentManager.getFragments();
        if (list.indexOf(fragment) <= -1) {
            fragmentManager.beginTransaction()
                    .add(R.id.container, fragment, tag)
                    .commit();
        } else {
            fragmentManager.beginTransaction()
                    .show(fragment)
                    .commit();
        }
    }

    private final Fragment getByTagFragment(String tag) {
        if (mFragmentActivity == null)
            return null;

        FragmentManager fragmentManager = mFragmentActivity.getSupportFragmentManager();
        return fragmentManager.findFragmentByTag(tag);
    }

    private final void hideFragment(Fragment fragment) {
        FragmentManager fragmentManager;
        fragmentManager = mFragmentActivity.getSupportFragmentManager();
        List<Fragment> list = fragmentManager.getFragments();

        if (list.indexOf(fragment) <= -1)
            return;

        fragmentManager.beginTransaction()
                .hide(fragment)
                .commit();
    }

    private final void logoutMe() {
        Multitone.getInstance().getAuth().logout();
        Intent mAuthActivity = new Intent(mFragmentActivity, AuthActivity.class);
        mAuthActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mAuthActivity.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        mAuthActivity.addCategory(Intent.CATEGORY_HOME);
        mFragmentActivity.startActivity(mAuthActivity);
        mFragmentActivity.finish();
    }
}

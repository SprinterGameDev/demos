package com.cgm.mysocialapp.controllers;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by admin on 07.10.15.
 */
public class AbstractController {

    protected AppCompatActivity mActivity;

    public AbstractController(AppCompatActivity activity) {
        mActivity = activity;
    }

    public void destroy() {
        mActivity = null;
        throw new UnsupportedOperationException("destroy not implemented", new Throwable());
    }

    public void saveInstance(Bundle outState) {
        throw new UnsupportedOperationException("saveInstance not implemented", new Throwable());
    }

    public void restoreInstance(Bundle savedInstance) {
        throw new UnsupportedOperationException("restoreInstance not implemented", new Throwable());
    }
}

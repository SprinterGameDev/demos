package com.cgm.mysocialapp.controllers;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.parse.ParseException;
import com.parse.SaveCallback;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Created by admin on 27.10.15.
 */
public final class CreatePostController extends AbstractController {
    public static final int PICK_PHOTO_ATTACH = 2133;
    private Fragment mFragment = null;
    private Bitmap mImagePost = null;

    public CreatePostController(Fragment fragment) {
        super(null);
        mFragment = fragment;
    }

    @Override
    public final void destroy() {
        mFragment = null;
        mImagePost = null;
    }

    public final void pickupImage() {
        Intent gallery = new Intent(Intent.ACTION_GET_CONTENT);
        gallery.setType("image/*");
        mFragment.startActivityForResult(gallery, PICK_PHOTO_ATTACH);
    }

    public final void applyAttachImage(Uri uri) {
        try {
            InputStream inputStream = mFragment.getActivity().getContentResolver().openInputStream(uri);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            mImagePost = BitmapFactory.decodeStream(inputStream, null, options);

        } catch(FileNotFoundException fileNotFound) {
            Multitone.getInstance()
                    .getDlgManager()
                    .openCommonDialog(mFragment.getFragmentManager(), fileNotFound.getMessage());
        }
    }

    public final void createPost(String message) {
        Multitone.getInstance()
                .getDlgManager()
                .openCommonProgressDialog(
                        mFragment.getFragmentManager(),
                        mFragment.getString(R.string.create_post_progress));
        Multitone.getInstance().getUser().createPost(message, mImagePost, mCallBackSavePost);
    }

    private final SaveCallback mCallBackSavePost = new SaveCallback() {
        @Override
        public void done(ParseException e) {
            Multitone.getInstance()
                    .getDlgManager().closeProgressDialog(mFragment.getFragmentManager());
            DialogFragment dialog = (DialogFragment) mFragment;
            dialog.dismiss();
        }
    };
}

package com.cgm.mysocialapp.auth;

import com.cgm.mysocialapp.models.UserModel;
import com.cgm.mysocialapp.utilits.callback.EventDispatcher;

/**
 * Created by admin on 03.10.15.
 */
public class AuthAbstract extends EventDispatcher {
    public static final String EVENT_AUTH_SUCCESS = "onAuthSuccess";
    public static final String EVENT_AUTH_ERROR = "onAuthError";

    protected UserModel mUser = null;

    public AuthAbstract() {
        super();
    }

    public void signIn() {

    }

    public void signUp(String... params) {

    }

    public void logout() {

    }

    public Boolean isActive() {
        return false;
    }

    public UserModel getUser() {
        return mUser;
    }
}

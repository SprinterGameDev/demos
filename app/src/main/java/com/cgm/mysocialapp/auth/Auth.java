package com.cgm.mysocialapp.auth;

import com.cgm.mysocialapp.models.UserModel;
import com.cgm.mysocialapp.utilits.callback.EventCallBack;

/**
 * Created by admin on 03.10.15.
 */
public final class Auth extends AuthAbstract {
    private AuthParse mAuthParse;

    public Auth() {
        super();
        mAuthParse = new AuthParse();// TODO пока только одна авторизация
        mAuthParse.addEventListener(AuthAbstract.EVENT_AUTH_ERROR, onError);
        mAuthParse.addEventListener(AuthAbstract.EVENT_AUTH_SUCCESS, onSuccess);
    }

    public final void signIn(String... params) {
        mAuthParse.signIn(params[0], params[1]);
    }

    @Override
    public final void signUp(String... params) {
        mAuthParse.signUp(params[0], params[1]);
    }

    public final void logout() {
        mAuthParse.logout();
    }

    public final Boolean isActive() {
        return mAuthParse.isActive();
    }

    public final UserModel getUser() {
        return mAuthParse.getUser();
    }

    private final EventCallBack onError = new EventCallBack() {
        @Override
        public void onEvent(int result) {
            dispatchEvent(AuthAbstract.EVENT_AUTH_ERROR, result);
        }
    };

    private final EventCallBack onSuccess = new EventCallBack() {
        @Override
        public void onEvent(int result) {
            dispatchEvent(AuthAbstract.EVENT_AUTH_SUCCESS, result);
        }
    };
}

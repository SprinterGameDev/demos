package com.cgm.mysocialapp.auth;

import com.cgm.mysocialapp.models.UserModel;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONObject;


/**
 * Created by admin on 03.10.15.
 */
public final class AuthParse extends AuthAbstract {
    public AuthParse() {
        super();
    }

    public void signIn(String login, String pass) {
        ParseUser.logInInBackground(login, pass, new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (e == null) {
                    mUser = new UserModel(user);
                    dispatchEvent(AuthAbstract.EVENT_AUTH_SUCCESS, 0);
                } else {
                    dispatchEvent(AuthAbstract.EVENT_AUTH_ERROR, e.getCode());
                }
            }
        });
    }

    @Override
    public final void logout() {
        ParseUser.logOutInBackground();
    }

    public final void signUp(String login, String pass) {
        final ParseUser user = new ParseUser();
        user.setUsername(login);
        user.setPassword(pass);

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    mUser = new UserModel(user);
                    dispatchEvent(AuthAbstract.EVENT_AUTH_SUCCESS, 0);
                } else {
                    dispatchEvent(AuthAbstract.EVENT_AUTH_ERROR, e.getCode());
                }
            }
        });
    }

    @Override
    public final Boolean isActive() {
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            mUser = new UserModel(currentUser);
            return true;
        }
        return false;
    }
}

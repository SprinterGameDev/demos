package com.cgm.mysocialapp;

import android.util.Log;

import com.cgm.mysocialapp.auth.Auth;
import com.cgm.mysocialapp.models.UserModel;

/**
 * Created by admin on 03.10.15.
 */
public final class Multitone {
    private static final String TAG = "Multitone";
    private static Multitone sInstance = null;

    private Auth mAuth = null;
    private DialogManager mDialogManager = null;

    public Multitone() {
        mAuth = new Auth();
        mDialogManager = new DialogManager();
    }

    public static Multitone getInstance() {
        if (sInstance == null)
            sInstance = new Multitone();
        return  sInstance;
    }

    public final Auth getAuth() {
        return mAuth;
    }

    public final DialogManager getDlgManager() {
        return mDialogManager;
    }

    public final UserModel getUser() {
        return mAuth.getUser();
    }
}

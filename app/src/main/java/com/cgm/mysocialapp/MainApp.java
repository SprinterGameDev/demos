package com.cgm.mysocialapp;

import android.app.Application;

import com.parse.Parse;
import com.shamanland.fonticon.FontIconTypefaceHolder;

/**
 * Created by admin on 25.10.15.
 */
public final class MainApp extends Application {

    @Override
    public final void onCreate() {
        super.onCreate();

        Parse.enableLocalDatastore(this);
        Parse.initialize(this, BuildConfig.parseAppID, BuildConfig.parseClientKey);

        FontIconTypefaceHolder.init(getAssets(), "fonts/fontawesome-webfont.ttf");
    }
}

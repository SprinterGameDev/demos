package com.cgm.mysocialapp.collections;

import com.cgm.mysocialapp.interfaces.SyncInterface;
import com.cgm.mysocialapp.models.PostItemModel;
import com.cgm.mysocialapp.utilits.callback.EventDispatcher;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by admin on 11.10.15.
 */
public class PostCollection extends EventDispatcher implements SyncInterface {
    public static final String EVENT_SUCCESS_LOAD = "onSuccess";
    public static final String EVENT_ERROR_LOAD = "onError";

    private static PostCollection sInstance = null;

    private ArrayList<PostItemModel> mCollection = new ArrayList<PostItemModel>();

    public PostCollection() {
    }

    public static PostCollection getInstance() {
        if (sInstance == null)
            sInstance = new PostCollection();
        return sInstance;
    }

    public final ArrayList<PostItemModel> getModels() {
        return mCollection;
    }

    public final void fetch() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("CollectionPost");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    mCollection.clear();
                    Iterator<ParseObject> itr = objects.iterator();

                    while (itr.hasNext()) {
                        mCollection.add(new PostItemModel(itr.next()));
                    }

                    Collections.reverse(mCollection);
                    dispatchEvent(EVENT_SUCCESS_LOAD, 0);
                } else {
                    dispatchEvent(EVENT_ERROR_LOAD, e.getCode());
                }
            }
        });
    }

    public final PostItemModel getById(String id) {
        Iterator<PostItemModel> iterator = mCollection.iterator();

        while(iterator.hasNext()) {
            PostItemModel model = iterator.next();
            if (model.getId() == id)
                return model;
        }
        return  null;
    }

    public final void fetchByUser(String userId) {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("CollectionPost");
        query.whereEqualTo("userObjId", userId);

        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    mCollection.clear();
                    Iterator<ParseObject> itr = objects.iterator();

                    while (itr.hasNext()) {
                        mCollection.add(new PostItemModel(itr.next()));
                    }

                    Collections.reverse(mCollection);
                    dispatchEvent(EVENT_SUCCESS_LOAD, 0);
                } else {
                    dispatchEvent(EVENT_ERROR_LOAD, e.getCode());
                }
            }
        });
    }
}

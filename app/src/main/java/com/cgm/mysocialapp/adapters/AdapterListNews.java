package com.cgm.mysocialapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgm.mysocialapp.Multitone;
import com.cgm.mysocialapp.R;
import com.cgm.mysocialapp.collections.PostCollection;
import com.cgm.mysocialapp.models.PostItemModel;
import com.cgm.mysocialapp.view.activity.NewsActivity;
import com.cgm.mysocialapp.view.layouts.ProfileLayout;
import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by admin on 01.11.15.
 */
public class AdapterListNews extends BaseAdapter {
    public static final String NAME_EXTRA_PARSE_OBJECT_ID = "parseObjId";

    private Activity mCtx;
    private LayoutInflater mInflater;
    private ArrayList<PostItemModel> mList = null;
    private ProfileLayout mProfileView = null;
    private Boolean mShowProfile = false;

    public AdapterListNews(Boolean showProfile, Activity context, PostCollection collection) {
        mShowProfile = showProfile;
        mCtx = context;
        mList = collection.getModels();

        mInflater = (LayoutInflater) mCtx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public final int getCount() {
        return mList.size() + (mShowProfile == true ? 1 : 0);
    }

    @Override
    public final Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        ListNewsHolder holder = null;
        View view = convertView;

        if (position == 0 && mShowProfile == true) {
            if (mProfileView == null || mProfileView.getView() == null)
                mProfileView = new ProfileLayout((AppCompatActivity) mCtx,
                        Multitone.getInstance().getAuth().getUser());

            return mProfileView.getView();
        }

        int modelIndex = position - (mShowProfile == true ? 1 : 0);

        if (modelIndex > mList.size())
            return view;

        final PostItemModel model = mList.get(modelIndex);

        if (view == null || view != null && view.getTag() == null) {
            view = mInflater.inflate(R.layout.layout_item_news, parent, false);
            ImageView userPhoto = (ImageView) view.findViewById(R.id.imgUserPhoto);
            TextView txtCreateAt = (TextView) view.findViewById(R.id.txtCreateAt);
            ImageView imgPost = (ImageView) view.findViewById(R.id.imgPost);
            TextView txtCaption = (TextView) view.findViewById(R.id.label);
            TextView txtMessage = (TextView) view.findViewById(R.id.txtMessage);
            Button btnComment = (Button) view.findViewById(R.id.btnComment);
            Button btnLikes = (Button) view.findViewById(R.id.btnLikes);
            Button btnShare = (Button) view.findViewById(R.id.btnShare);

            holder = new ListNewsHolder(userPhoto, txtCaption, txtCreateAt, imgPost, txtMessage,
                    btnComment, btnLikes, btnShare);

            view.setTag(holder);
        } else {
            holder = (ListNewsHolder) view.getTag();
        }

        if (holder != null) {

            holder.setAvatar(model.getAvatar());
            holder.setTextCaption(model.getName());
            holder.setCreateAt(model.getCreateAt());
            holder.setPhoto(model.getPhoto());
            holder.setTextMessage(model.getMessage());
            holder.getBtnLikes().setText(String.valueOf(model.getLikeCount()));
            holder.getBtnShare().setText(String.valueOf(model.getShareCount()));
            holder.getBtnComment().setText(String.valueOf(model.getCommentCount()));

            holder.getBtnComment().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showActivityNews(model.getParseObject());
                }
            });
        }
        return view;
    }

    private final void showActivityNews(ParseObject obj) {
        Intent mNews = new Intent(mCtx, NewsActivity.class);

        mNews.putExtra(NAME_EXTRA_PARSE_OBJECT_ID, obj.getObjectId());
        mNews.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mCtx.startActivity(mNews);
    }

    public final void destroy() {
        if (mProfileView != null)
            mProfileView.destroy();
        mProfileView = null;
    }
}

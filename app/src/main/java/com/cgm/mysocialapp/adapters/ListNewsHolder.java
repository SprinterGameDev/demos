package com.cgm.mysocialapp.adapters;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.parse.ParseException;
import com.parse.ParseFile;

/**
 * Created by admin on 12.10.15.
 */
public final class ListNewsHolder {
    private ImageView mUserPhoto = null;
    private TextView mCaption = null;
    private TextView mCreateAt = null;
    private ImageView mPhoto = null;
    private TextView mMessage = null;
    private Button mBtnComments = null;
    private Button mBtnLikes = null;
    private Button mBtnShare = null;

    public ListNewsHolder(ImageView userPhoto, TextView caption, TextView createAt, ImageView photo, TextView message,
                          Button comments, Button likes, Button share) {
        this.mUserPhoto = userPhoto;
        this.mCaption = caption;
        this.mCreateAt = createAt;
        this.mPhoto = photo;
        this.mMessage = message;
        this.mBtnComments = comments;
        this.mBtnLikes = likes;
        this.mBtnShare = share;
    }

    public final void setAvatar(Bitmap photo) {
        BitmapUtils.clearImageView(mUserPhoto);

        if (photo == null)
            return;

        mUserPhoto.setImageBitmap(
                BitmapUtils.getCroppedBitmap(photo)
        );
    }

    public final void setTextCaption(String text) {
        if (mCaption != null)
            mCaption.setText(text);
    }

    public final void setPhoto(ParseFile file) {
        BitmapUtils.clearImageView(mPhoto);

        if (file == null)
            return;

        try {
            byte[] bitmapData = file.getData();

            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            mPhoto.setImageBitmap(bitmap);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public final void setTextMessage(String text) {
        if (mMessage != null)
            mMessage.setText(text);
    }

    public final void setCreateAt(String text) {
        if (mMessage != null)
            mCreateAt.setText(text);
    }

    public final Button getBtnComment() {
        return mBtnComments;
    }

    public final Button getBtnLikes() {
        return mBtnLikes;
    }

    public final Button getBtnShare() {
        return mBtnShare;
    }
}

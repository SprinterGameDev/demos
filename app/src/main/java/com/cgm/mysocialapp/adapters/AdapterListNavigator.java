package com.cgm.mysocialapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cgm.mysocialapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by admin on 17.10.15.
 */
public final class AdapterListNavigator extends BaseAdapter {
    private Context mCtx;
    private LayoutInflater mInflater;
    private ArrayList<String> mList = null;
    private List<ListNavigatorHolder> mListHolder= new ArrayList<ListNavigatorHolder>();

    public AdapterListNavigator(Context context, ArrayList<String> list) {
        mCtx = context;
        mList = list;
        mInflater = (LayoutInflater) mCtx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public final int getCount() {
        return mList.size();
    }

    @Override
    public final Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public final long getItemId(int position) {
        return position;
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        ListNavigatorHolder holder = null;
        View view = convertView;

        if (view == null) {
            view = mInflater.inflate(R.layout.navigator_list_item, parent, false);
            TextView txtCaption = (TextView) view.findViewById(R.id.label);
            holder = new ListNavigatorHolder(txtCaption);
            view.setTag(holder);
            mListHolder.add(holder);
        } else {
            holder = (ListNavigatorHolder) view.getTag();
        }

        holder.setTextCaption(mList.get(position));
        return view;
    }

}

package com.cgm.mysocialapp.adapters;

import android.view.View;
import android.widget.TextView;

/**
 * Created by admin on 17.10.15.
 */
public final class ListNavigatorHolder {
    private TextView mTxtCaption = null;

    public ListNavigatorHolder(TextView txtCaption) {
        mTxtCaption = txtCaption;
    }

    public final void setTextCaption(String text) {
        if (mTxtCaption != null)
            mTxtCaption.setText(text);
    }
}

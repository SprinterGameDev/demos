package com.cgm.mysocialapp;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.cgm.mysocialapp.view.fragments.AlertDialogFragment;
import com.cgm.mysocialapp.view.fragments.CreatePostDialogFragment;
import com.cgm.mysocialapp.view.fragments.ProgressDialogFragment;

/**
 * Created by admin on 24.10.15.
 */
public final class DialogManager {
    public final void openCreatePost(FragmentManager manager) {
        if (manager.findFragmentByTag("DlgCreatePost") != null)
            return;

        CreatePostDialogFragment dlgPost = new CreatePostDialogFragment();
        dlgPost.show(manager, "DlgCreatePost");
        dlgPost.setRetainInstance(true);
    }

    public final void openCommonDialog(FragmentManager manager, String message) {
            AlertDialogFragment mDlgAlert = new AlertDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putString(
                    AlertDialogFragment.KEY_MESSAGE,
                    message
            );
        mDlgAlert.setArguments(bundle);
        mDlgAlert.show(manager, "mDlgAlert");
    }

    public final void openCommonProgressDialog(FragmentManager manager, String caption) {
        closeProgressDialog(manager);

        ProgressDialogFragment mDlgProgress = new ProgressDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(
                ProgressDialogFragment.KEY_CAPTION,
                caption
        );
        mDlgProgress.setArguments(bundle);
        mDlgProgress.show(manager, "mDlgProgress");
    }

    public final void closeProgressDialog(FragmentManager manager) {
        DialogFragment mDlgProgress = (DialogFragment) manager.findFragmentByTag("mDlgProgress");
        if (mDlgProgress != null)
            mDlgProgress.dismiss();
    }
}

package com.cgm.mysocialapp.utilits.callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by admin on 02.10.15.
 */
public class EventDispatcher {
    private Map<String, ArrayList<EventCallBack>> mListeners = new HashMap<String, ArrayList<EventCallBack>>();


    public void EventDispatcher() {

    }

    public void addEventListener(String event, EventCallBack callBack) {
        ArrayList list = mListeners.get(event);
        if (list == null) {
            list = new ArrayList<EventCallBack>();
            mListeners.put(event, list);
        }

        if (list.indexOf(callBack) <= 0)
            list.add(callBack);
    }

    public void removeEventListener(String event, EventCallBack callBack) {
        ArrayList list = mListeners.get(event);
        if (list == null)
            return;

        list.remove(callBack);
    }

    public void dispatchEvent(String event, int result) {
        ArrayList list = mListeners.get(event);
        if (list == null)
            return;

        Iterator<Object> iterator = list.iterator();

        while(iterator.hasNext()) {
            EventCallBack callBack = (EventCallBack) iterator.next();
            callBack.onEvent(result);
        }
    }
}

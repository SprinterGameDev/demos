package com.cgm.mysocialapp.models;

import android.graphics.Bitmap;

import com.cgm.mysocialapp.interfaces.SyncInterface;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.cgm.mysocialapp.utilits.callback.EventDispatcher;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by admin on 03.10.15.
 */
public class PostItemModel extends EventDispatcher implements SyncInterface {
    private String mId = "";
    private String mNameUser = "";
    private String mAvatar = null;
    private String mUserId = "";
    private String mMessage = "";
    private ParseFile mPhoto = null;
    private List<String> mUsersLiked = null;
    private List<String> mUsersCommented = null;
    private List<String> mUsersShare = null;
    private ParseObject mBaseModel = null;
    private Date mCreateAt = null;

    public PostItemModel(ParseObject object) {
        super();
        mBaseModel = object;

        mId = object.getObjectId();
        mNameUser = object.getString("name");
        mAvatar = object.getString("avatar");
        mUserId = object.getString("userObjId");
        mMessage = object.getString("message");
        mPhoto = object.getParseFile("image");
        mUsersLiked = object.getList("likes");
        mUsersCommented = object.getList("comments");
        mCreateAt = object.getCreatedAt();
    }

    public final String getId() {
        return mId;
    }

    public final String getName() {
        return  mNameUser;
    }

    public final Bitmap getAvatar() {
        return  mAvatar != null ? BitmapUtils.decodeBase64(mAvatar) : null;
    }

    public final String getUserId() {
        return  mUserId;
    }

    public final String getMessage() {
        return mMessage;
    }

    public final ParseFile getPhoto() {
        return mPhoto;
    }

    public final int getLikeCount() {
        return mUsersLiked == null ? 0 : mUsersLiked.size();
    }

    public final int getShareCount() {
        return mUsersShare == null ? 0 : mUsersShare.size();
    }

    public final int getCommentCount() {
        return mUsersCommented == null ? 0 : mUsersCommented.size();
    }

    public final String getCreateAt() {
        return mCreateAt == null ? "" : mCreateAt.toString();
    }

    public final void addLike() {
    }

    public final void addComment(String mMessage) {
    }

    public final void addShare(String subMessage) {
    }

    public final ParseObject getParseObject() {
        return mBaseModel;
    }

    public final void fetch() {

    }
}

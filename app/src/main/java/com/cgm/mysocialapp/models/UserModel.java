package com.cgm.mysocialapp.models;

import android.graphics.Bitmap;

import com.cgm.mysocialapp.interfaces.SyncInterface;
import com.cgm.mysocialapp.utilits.BitmapUtils;
import com.cgm.mysocialapp.utilits.callback.EventDispatcher;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by admin on 03.10.15.
 */
public class UserModel extends EventDispatcher implements SyncInterface {
    public static final String EVENT_CHANGE_AVATAR = "onChangeAvatar";

    private ParseUser mParseUser = null;

    public UserModel(ParseUser parseUser) {
        super();
        mParseUser = parseUser;
    }

    public UserModel() {
        super();
    }

    public final ParseUser getParseUser() {
        return mParseUser;
    }

    public final String getId() {
        return mParseUser != null ? mParseUser.getObjectId() : null;
    }

    public final String getName() {
        String name = mParseUser != null ? mParseUser.getString("name") : "";
        if (name == null)
            name = "";

        return name;
    }

    public final void setName(String name) {
        mParseUser.put("name", name);
        mParseUser.saveInBackground();
    }

    public final Bitmap getAvatar() {
        return mParseUser != null && mParseUser.getString("avatar") != null ?
                BitmapUtils.decodeBase64(mParseUser.getString("avatar")) : null;
    }

    public final void setAvatar(String base64) {
        mParseUser.put("avatar", base64);
        mParseUser.saveInBackground();
        dispatchEvent(EVENT_CHANGE_AVATAR, 0);
    }

    public final void createPost(String message, Bitmap image, final SaveCallback callBack) {
        if (mParseUser == null)
            return;

        final ParseObject dataObject = new ParseObject("CollectionPost");

        if (message != null)
            dataObject.put("message", message);

        if (image != null) {
            ParseFile file = new ParseFile("image", BitmapUtils.getBytes(image));
            dataObject.put("image", file);
        }

        dataObject.put("name", mParseUser.getString("name"));
        dataObject.put("avatar", mParseUser.getString("avatar"));
        dataObject.put("userObjId", mParseUser.getObjectId());

        dataObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                List<String> listPost = mParseUser.getList("posts");
                if (listPost == null)
                    listPost = new ArrayList<>();
                listPost.add(dataObject.getObjectId());
                mParseUser.put("posts", listPost);
                mParseUser.saveInBackground(callBack);
            }
        });
    }

    public void fetch() {

    }

}
